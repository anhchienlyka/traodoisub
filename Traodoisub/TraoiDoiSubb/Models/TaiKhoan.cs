﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TraoiDoiSubb.Models
{
    public class TaiKhoan
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public IEnumerable<Account> Accounts { get; set; }
    }
}
