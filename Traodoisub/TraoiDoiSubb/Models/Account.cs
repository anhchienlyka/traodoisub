﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TraoiDoiSubb.Models
{
    public class Account
    {
        public int Id { get; set; }
        public string IDToken { get; set; }
        public int TaiKhoanId { get; set; }
        public string User { get; set; }
        public long Xudie { get; set; }
        public bool Status { get; set; }
        public long XuBanDau { get; set; }
        public TaiKhoan TaiKhoan { get; set; }
    }
}
