﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TraoiDoiSubb.Models
{
    public class AccountViewModel
    {
        public string User { get; set; }
        public long Xu { get; set; }
        public long Xudie { get; set; }
        public long XuBanDau { get; set; }
        public long XuTang { get; set; }
        public bool Status { get; set; }
    }
}
