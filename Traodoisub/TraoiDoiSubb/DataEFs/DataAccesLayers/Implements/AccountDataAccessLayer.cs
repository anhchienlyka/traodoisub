﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TraoiDoiSubb.DataEFs.DataAccesLayers.Interfaces;
using TraoiDoiSubb.DataEFs.DataContexts;
using TraoiDoiSubb.Models;

namespace TraoiDoiSubb.DataEFs.DataAccesLayers.Implements
{
    public class AccountDataAccessLayer : IAccountDataAccessLayer
    {
        private readonly TraodoisubContext _context;
        public AccountDataAccessLayer(TraodoisubContext context)
        {
            _context = context;
        }

        public void AddAcount(Account account)
        {
            _context.Accounts.Add(account);
            _context.SaveChanges();
        }

        public void DeleteAccount(int id)
        {
            var acc = _context.Accounts.Find(id);
            _context.Accounts.Remove(acc);
            _context.SaveChanges();
        }

        public Account FindAccount(int id)
        {
            return _context.Accounts.Find(id);
        }

        public List<Account> GetAllListAccount(int taiKhoanId)
        {
            return _context.Accounts.Where(x => x.TaiKhoanId == taiKhoanId).ToList();
        }

        public List<string> GetAllListIdToken(int taiKhoanId)
        {
            var listIdToken = new List<string>();
            var listAccout = _context.Accounts.Where(x => x.TaiKhoanId == taiKhoanId).ToList();
            foreach (var item in listAccout)
            {
                var token = item.IDToken;
                listIdToken.Add(token);
            }
            return listIdToken;
        }

        public void UpdateAccout(Account account)
        {
            _context.Accounts.Update(account);
            _context.SaveChanges();
        }

        public void UpdateListAccount(List<Account> accounts)
        {
            _context.UpdateRange(accounts);
            _context.SaveChanges();
        }
    }
}
