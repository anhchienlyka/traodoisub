﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TraoiDoiSubb.DataEFs.DataAccesLayers.Interfaces;
using TraoiDoiSubb.DataEFs.DataContexts;
using TraoiDoiSubb.Models;

namespace TraoiDoiSubb.DataEFs.DataAccesLayers.Implements
{
    public class TaikhoanDataAccessLayer : ITaikhoanDataAccessLayer
    {
        private readonly TraodoisubContext _context;
        public TaikhoanDataAccessLayer(TraodoisubContext context)
        {
            _context = context;
        }
        public bool CheckExitAccount(TaiKhoan taiKhoan)
        {
            var listTaikhoan = _context.TaiKhoans.ToList();
            foreach (var item in listTaikhoan)
            {
                if (taiKhoan.UserName == item.UserName && taiKhoan.Password == item.Password)
                {
                    return true;
                }
            }
            return false;
        }

        public TaiKhoan GetInforTaiKhoan(string userName)
        {
            return _context.TaiKhoans.Where(x => x.UserName == userName).FirstOrDefault();
        }
    }
}
