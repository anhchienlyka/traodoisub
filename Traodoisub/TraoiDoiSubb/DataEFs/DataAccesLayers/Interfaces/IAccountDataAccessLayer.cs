﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TraoiDoiSubb.Models;

namespace TraoiDoiSubb.DataEFs.DataAccesLayers.Interfaces
{
    public interface IAccountDataAccessLayer
    {
        List<Account> GetAllListAccount(int taiKhoanId);
        List<string> GetAllListIdToken(int taiKhoanId);
        void AddAcount(Account account);
        void DeleteAccount(int id);
        void UpdateAccout(Account account);
        void UpdateListAccount(List<Account> accounts);

        Account FindAccount(int id);
    }
}
