﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TraoiDoiSubb.Models;

namespace TraoiDoiSubb.DataEFs.DataAccesLayers.Interfaces
{
  public  interface ITaikhoanDataAccessLayer
    {
        bool CheckExitAccount(TaiKhoan taiKhoan);
        TaiKhoan GetInforTaiKhoan(string userName);
    }
}
