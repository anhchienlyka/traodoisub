﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TraoiDoiSubb.Models;

namespace TraoiDoiSubb.DataEFs.DataContexts
{
    public class TraodoisubContext : DbContext
    {
        public TraodoisubContext()
        {

        }
        public TraodoisubContext(DbContextOptions<TraodoisubContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TaiKhoan>(taikhoan =>
            {
                taikhoan.HasKey(x => x.Id);
            });

            modelBuilder.Entity<Account>(account =>
            {
                account.HasKey(x => x.Id);
                account.HasOne(x => x.TaiKhoan).WithMany(x => x.Accounts).HasForeignKey(x => x.TaiKhoanId);

            });
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<TaiKhoan> TaiKhoans { get; set; }
    }
}
