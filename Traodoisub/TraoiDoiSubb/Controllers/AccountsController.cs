﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TraoiDoiSubb.DataEFs.DataAccesLayers.Interfaces;
using TraoiDoiSubb.Models;

namespace TraoiDoiSubb.Controllers
{
  //  [Route("account")]
    public class AccountsController : Controller
    {
        private readonly ITaikhoanDataAccessLayer _taikhoanDataAccessLayer;
        private readonly INotyfService _notyf;
        public AccountsController(ITaikhoanDataAccessLayer taikhoanDataAccessLayer, INotyfService notyf)
        {
            _taikhoanDataAccessLayer = taikhoanDataAccessLayer;
            _notyf = notyf;
        }
        //[Route("")]
        //[Route("index")]
        //[Route("~/")]
        public IActionResult Index()
        {
            return View();
        }


       
        [HttpPost]
        public IActionResult Login(TaiKhoan taiKhoan)
        {
            if (taiKhoan.UserName != null && taiKhoan.Password != null&& _taikhoanDataAccessLayer.CheckExitAccount(taiKhoan))
            {   
                    var taikhoanData = _taikhoanDataAccessLayer.GetInforTaiKhoan(taiKhoan.UserName);
                    HttpContext.Session.SetString("fullname", taikhoanData.FullName);
                    HttpContext.Session.SetString("username", taikhoanData.UserName);
                    _notyf.Success("Đăng nhập thành công");
                    return RedirectToAction("Index", "Home");   
            }
            else
            {
            
                return RedirectToAction("Index");
              
            }
        }

        [Route("logout")]
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("username");
            return RedirectToAction("Index");
        }
    }
}
