﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TraoiDoiSubb.DataEFs.DataAccesLayers.Interfaces;
using TraoiDoiSubb.Models;

namespace TraoiDoiSubb.Controllers
{

    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAccountDataAccessLayer _account;
        private readonly INotyfService _notyf;
        private readonly ITaikhoanDataAccessLayer _taikhoanDataAccessLayer;
        public HomeController(ILogger<HomeController> logger, IAccountDataAccessLayer account, INotyfService notyf, ITaikhoanDataAccessLayer taikhoanDataAccessLayer)
        {
            _logger = logger;
            _taikhoanDataAccessLayer = taikhoanDataAccessLayer;
            _account = account;
            _notyf = notyf;
        }

        public IActionResult IndexAsync()
        {
            var userName = HttpContext.Session.GetString("username");
            var taiKhoan = _taikhoanDataAccessLayer.GetInforTaiKhoan(userName);
            var listAccount = _account.GetAllListAccount(taiKhoan.Id);
            listAccount = listAccount.OrderByDescending(x => x.XuBanDau).ToList();
            return View(listAccount);
        }


        public async Task<IActionResult> LoadData()
        {
            var userName = HttpContext.Session.GetString("username");
            var taiKhoan = _taikhoanDataAccessLayer.GetInforTaiKhoan(userName);

            var lines = _account.GetAllListIdToken(taiKhoan.Id);
            var listAccout = _account.GetAllListAccount(taiKhoan.Id);
            string url = "https://traodoisub.com/api";
            var reservationList = new List<AccoutVM>();
            var listAccountViewModels = new List<AccountViewModel>();
            using (var httpClient = new HttpClient())
            {
                foreach (string line in lines)
                {
                    using (var response = await httpClient.GetAsync(url + string.Format("/?fields=profile&access_token={0}}}", line)))
                    {
                        var apiResponse = await response.Content.ReadAsStringAsync();
                        var data = JsonConvert.DeserializeObject<Response>(apiResponse);
                        if (data.Data != null) { reservationList.Add(data.Data); }
                    }
                }
            }
            reservationList = reservationList.ToList();
            for (int i = 0; i < reservationList.Count; i++)
            {
                var accountViewModel = new AccountViewModel()
                {
                    XuBanDau = listAccout[i].XuBanDau,
                    Status = listAccout[i].Status,
                    Xu = reservationList[i].Xu,
                    Xudie = reservationList[i].Xudie,
                    XuTang = reservationList[i].Xu - listAccout[i].XuBanDau,
                    User = reservationList[i].User
                };
                listAccountViewModels.Add(accountViewModel);
                listAccout[i].XuBanDau = reservationList[i].Xu;
                listAccout[i].Xudie = reservationList[i].Xudie;
            }
            _account.UpdateListAccount(listAccout);
            _notyf.Success("Tải dữ liệu thành công");
            listAccountViewModels = listAccountViewModels.Where(x => x.Status == true).ToList();
            return View(listAccountViewModels);
        }


        public IActionResult Update(int id)
        {
            var account = _account.FindAccount(id);
            if (account.Status == true)
            {
                account.Status = false;
            }
            else { account.Status = true;
            }
            _account.UpdateAccout(account);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> AddAsync(string token)
        {
            var userName = HttpContext.Session.GetString("username");
            var taiKhoan = _taikhoanDataAccessLayer.GetInforTaiKhoan(userName);
            var lines = _account.GetAllListIdToken(taiKhoan.Id);
            if (lines.Contains(token))
            {
                _notyf.Warning("IDToken đã tồn tại");
                return RedirectToAction("Index");
            }
            string url = "https://traodoisub.com/api";
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(url + string.Format("/?fields=profile&access_token={0}}}", token)))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Response>(apiResponse);
                    if (data.Data == null)
                    {
                        _notyf.Error("IDToken không tồn tại");
                        return RedirectToAction("Index");
                    }

                    var account = new Account()
                    {
                        IDToken = token,
                        XuBanDau = data.Data.Xu,
                        Xudie = data.Data.Xudie,
                        User = data.Data.User,
                        TaiKhoanId = taiKhoan.Id
                    };
                    _account.AddAcount(account);
                    _notyf.Success("Thêm mới IDToken thành công");
                }
            }
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            _account.DeleteAccount(id);
            _notyf.Success("Xóa tài khoản thành công");
            return RedirectToAction("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
